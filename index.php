<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal('Shaun');
echo "Name : ". $sheep->name."<br>";
echo "Legs : ". $sheep->legs."<br>";
echo "Cold Blooded : ". $sheep->cold_blooded."<br><br>";

$frog1 = new Frog('Buduk');
echo "Name : ". $frog1->name."<br>";
echo "Legs : ". $sheep->legs."<br>";
echo "Cold Blooded : ". $sheep->cold_blooded."<br>";
echo $frog1->jump()."<br><br>";

$ape1 = new Ape('Kera Sakti');
echo "Name : ". $ape1->name."<br>";
echo "Legs : ". $ape1->legs."<br>";
echo "Cold Blooded : ". $sheep->cold_blooded."<br>";
echo $ape1->yell();

?>